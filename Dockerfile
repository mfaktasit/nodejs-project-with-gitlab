# Use the official Node.js 14 image as a base
FROM node:14

# Set the working directory in the container
WORKDIR /app

# Copy everything from the current directory to the working directory
COPY . .

# Install dependencies
RUN npm install

# Expose port 5000
EXPOSE 5000

# Command to run the application
CMD ["node", "app.js"]
