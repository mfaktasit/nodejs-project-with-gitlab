// Import required modules
import { getProducts, getProduct, createProduct, updateProduct, deleteProduct } from './controllers/products.js';

// Define Lambda handler function
export const handler = async (event) => {
    try {
        // Extract HTTP method and resource path from the event
        const httpMethod = event.httpMethod;
        const resourcePath = event.resource;

        // Define response object
        let response;

        // Handle GET request
        if (httpMethod === 'GET') {
            // Handle GET /api/products and GET /api/products/{productID}
            if (resourcePath === '/api/products') {
                response = await getProducts();
            } else if (resourcePath.startsWith('/api/products/')) {
                const productID = resourcePath.split('/').pop();
                response = await getProduct(productID);
            } else {
                // Return 404 for unknown paths
                response = {
                    statusCode: 404,
                    body: JSON.stringify({ message: 'Resource not found' })
                };
            }
        }

        // Handle POST request
        else if (httpMethod === 'POST' && resourcePath === '/api/products') {
            const requestBody = JSON.parse(event.body);
            response = await createProduct(requestBody);
        }

        // Handle PUT request
        else if (httpMethod === 'PUT' && resourcePath.startsWith('/api/products/')) {
            const productID = resourcePath.split('/').pop();
            const requestBody = JSON.parse(event.body);
            response = await updateProduct(productID, requestBody);
        }

        // Handle DELETE request
        else if (httpMethod === 'DELETE' && resourcePath.startsWith('/api/products/')) {
            const productID = resourcePath.split('/').pop();
            response = await deleteProduct(productID);
        }

        // Return 405 for unsupported methods
        else {
            response = {
                statusCode: 405,
                body: JSON.stringify({ message: 'Method not allowed' })
            };
        }

        return response;
    } catch (error) {
        console.error('Error:', error);
        return {
            statusCode: 500,
            body: JSON.stringify({ message: 'Internal server error' })
        };
    }
};